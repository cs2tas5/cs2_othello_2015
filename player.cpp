#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 * asdfasdf
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    // Create a new board object.

    board = Board();

    // Set the sides for the player and its opponent.
    playerside = side;
    if (side == BLACK)
    {
        oppside = WHITE;
    }
    else
    {
        oppside = BLACK;
    }

	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			if(i == 0)
			{
				scores[i][j] = 1;
			}
			else if(j == 0)
			{
				scores[i][j] = 1;
			}
			else if(i == 7)
			{
				scores[i][j] = 1;
			}
			else if(j == 7)
			{
				scores[i][j] = 1;
			}
			else
			{
				scores[i][j] = 0;
			}
		}
	}
	scores[0][0] = 3;
	scores[0][7] = 3;
	scores[7][0] = 3;
	scores[7][7] = 3;
	scores[0][1] = -2;
	scores[1][0] = -2;
	scores[1][1] = -3;
	scores[0][6] = -2;
	scores[1][7] = -2;
	scores[1][6] = -3;
	scores[6][0] = -2;
	scores[7][1] = -2;
	scores[6][1] = -3;
	scores[7][6] = -2;
	scores[6][7] = -2;
	scores[6][6] = -3;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Given the internal board state and the internal size variable, determines
 * which moves are valid for the player and returns all of the valid moves
 * as a vector of Move* objects.
 */

 vector<Move*> Player::validMoves(Board board, Side side)
 {
    vector<Move*> valid;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            Move* move = new Move(i, j); 
            if (board.checkMove(move, side))
            {
                valid.push_back(move);
            }
        }
    }
    return valid;
 }

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */

    // Update the board state to account for the opponent's last move.
    board.doMove(opponentsMove, oppside);

    // Check to see if the game is over
    if (!board.isDone())
    {
        // Check to see if the player's side has valid moves.
        if (board.hasMoves(playerside))
        {
            // Play a move. Right now, player just plays a random move.
            //vector<Move*> moves = validMoves();
            //board.doMove(moves[0], playerside);
            //return moves[0];
            
            //Testing heuristics
            // Move * res = heuristic(playerside);
            // board.doMove(res, playerside);
            // return res;

            // Minimax algorithm
            Move *res = minimax();
            board.doMove(res, playerside);
            return res;
            
        }
        else
        {
            // The player has no moves, but the other side does, so we have to
            // return NULL.
            return NULL;
        }
    }
    return NULL;
}

Move *Player::heuristic(Side side)
{
	int x, y;
	vector<Move*> moves = validMoves(board, side);
	Move * res = moves[0];
	x = moves[0]->x;
	y = moves[0]->y;
	int curscore = scores[x][y];
	for(int i = 1; i <  int(moves.size()); i++)
	{
		x = moves[i]->x;
		y = moves[i]->y;
		if(scores[x][y] > curscore)
		{
			res = moves[i];
			curscore = scores[x][y];
		}

	}
	x = res->x;
	y = res->y;
	//std::cerr<<x<<" "<<y<<std::endl;
	return res;
}


Move *Player::minimax()
{
    // Store the possible player moves on this turn.
    vector<Move*> playermoves = validMoves(board, playerside);

    // Holds the minimum score possible from an opponent's move from any of
    // player's moves.
    vector<int> minscores;

    // For each move the player can make...
    for (unsigned int i = 0; i < playermoves.size(); i++)
    {
        // Consider all of the moves an opponent can make.

        // Make a board with that which is the current board with that move
        // made
        Board *test_board = board.copy();
        test_board->doMove(playermoves[i], playerside);

        // Find the opponent's possible move from this move.
        vector<Move*> oppmoves = validMoves(*test_board, oppside);

        int min_score = 65;

        // For each of the opponent's moves...
        for (unsigned int j = 0; j < oppmoves.size(); j++)
        {
            // Make that move on the board.
            Board *test_board2 = test_board->copy();
            test_board2->doMove(oppmoves[j], oppside);

            // Calculate the score.
            int score = test_board2->count(playerside) - test_board2->count(oppside);
            // If this score is smaller than the current minimum, update it.
            if (score < min_score)
            {
                min_score = score;
            }
        }

        // Add our best opponent moves for each move we make into our vectors
        // of scores minscore.
        minscores.push_back(min_score);
    }

    // Find the move with the maximum minimum score.
    int best_index = 0;
    int move_score = minscores[0];

    if (minscores.size() > 1)
    {
        for (unsigned int i = 1; i < minscores.size(); i++)
        {
            if (minscores[i] > move_score)
            {
                move_score = minscores[i];
                best_index = i;
            }
        }
    }
    // for (unsigned int i = 1; i < minscores.size(); i++)
    // {
    //     if (minscores[i] < move_score)
    //     {
    //         move_score = minscores[i];
    //         best_index = i;
    //     }
    // }

    return playermoves[best_index];
}

void Player::setBoard(Board nboard)
{
    board = *nboard.copy();
}