#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

private:
	Board board;
	Side playerside;
	Side oppside;

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    Move *heuristic(Side side);

    Move *minimax();

    void setBoard(Board nboard);

    vector<Move*> validMoves(Board board, Side side);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    int scores[8][8];
};

#endif
